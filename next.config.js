/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['kuymakan-food.herokuapp.com', 'storage.googleapis.com']
  }
}

module.exports = nextConfig
