import Router from "next/router";
import Head from "next/head";
import Logo from "../Components/Logo";
import { Button, Flex } from "@chakra-ui/react";

import withoutAuth from "../Components/Utils/withoutAuth";

const Home = () => {
  const handleRegister = () => {
    Router.push("/register");
  };
  const handleLogin = () => {
    Router.push("/login");
  };

  return (
    <>
      <Head>
        <title>Homepage</title>
      </Head>
      <Flex height="100%" direction="column" justifyContent="center">
        <Logo />
        <Flex direction="column" padding="4rem">
          <Button
            marginTop="2.0rem"
            color="white"
            bg="primary"
            onClick={handleRegister}
          >
            Register
          </Button>
          <Button
            marginTop="1.0rem"
            color="primary"
            bg="white"
            onClick={handleLogin}
          >
            Login
          </Button>
        </Flex>
      </Flex>
    </>
  );
};

export default withoutAuth(Home);
