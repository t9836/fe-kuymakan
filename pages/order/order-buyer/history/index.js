import Head from "next/head";
import Cookies from "js-cookie";
import { useEffect, useState } from "react";
import Logo from "../../../../Components/Logo";
import { Flex, OrderedList, Spinner } from "@chakra-ui/react";
import BottomNav from "../../../../Components/Navbar";
import withAuth from "../../../../Components/Utils/withAuth";
import { getAllOrderBuyer } from "../../../../services/order";
import OrderHistoryCard from "../../../../Components/Person/OrderHistoryCard";

const OrderHistory = () => {

  const [historyData, setHistoryData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const getHistory = async () => {
    let response = await getAllOrderBuyer(Cookies.get('username'));
    response = response.data
    const sortedData = response.data.sort(
      (item1, item2) => item2.id - item1.id
    );
    setHistoryData(sortedData);
    setIsLoading(false);
  }

  useEffect(() => {
    getHistory();
  }, [])


  return (
    <>
      <Head>
        <title>Order History</title>
      </Head>
      <Logo />
      {
        isLoading ? 
        <Flex justifyContent='center' mt='50px'>
          <Spinner /> 
        </Flex>: 
        
        <Flex flexDirection={"column"} mx="4rem" paddingBottom="10rem">
        {historyData && historyData.map((history, idx) => {
          return <OrderHistoryCard key={idx} data={history} />
        })}
      </Flex>
      }
      
      <BottomNav />
    </>
  );
};

export default withAuth(OrderHistory);
