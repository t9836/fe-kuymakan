import Head from "next/head";
import { useState } from "react";
import { useRouter } from "next/router";
import Logo from "../../../../../Components/Logo";
import BottomNav from "../../../../../Components/Navbar";
import Form from "../../../../../Components/Form";
import FilePicker from "chakra-ui-file-picker";
import { Flex, Box, Text, Button, useToast } from "@chakra-ui/react";

import { postCreateReview } from "../../../../../services/review";
import withAuth from "../../../../../Components/Utils/withAuth";

const OrderReview = () => {
  const [review, setReview] = useState("");
  const [files, setFiles] = useState(null);
  const [progress, setProgress] = useState(null);

  const [isUploading, setIsUploading] = useState(false);

  const toast = useToast({
    position: "top",
  });

  const router = useRouter();

  const { id: orderId } = router.query;

  if (!router.isReady) {
    return <></>;
  }

  if (isNaN(parseInt(orderId))) {
    router.push("/");
    return;
  }

  const handleClick = async () => {
    const errorList = [];

    if (!files || files.length == 0) {
      errorList.push("Video harus ada untuk submit!");
    }

    if (files?.length > 1) {
      errorList.push("File tidak boleh lebih dari satu!");
    }

    if (!review) {
      errorList.push("Review harus diisi!");
    }

    if (review && review.length <= 5) {
      errorList.push("Review harus lebih banyak dari 5 huruf!");
    }

    if (errorList.length > 0) {
      errorList.forEach((message) => {
        toast({
          description: message,
          status: "error",
        });
      });
      return;
    }

    const video = files[0];
    const videoFileType = video.type;
    const videoSizeInMb = Math.round(video.size / 1024 / 1000);

    const ALLOWED_MIME_TYPE = ["video/quicktime", "video/mp4"];

    if (videoSizeInMb > 10) {
      errorList.push("Video should not be bigger than 10MB!");
    }

    if (!ALLOWED_MIME_TYPE.includes(videoFileType)) {
      errorList.push("Video needs to be .mov or .mp4!");
    }

    if (errorList.length > 0) {
      errorList.forEach((message) => {
        toast({
          description: message,
          status: "error",
        });
      });
      return;
    }

    const formDataPayload = new FormData();
    formDataPayload.append("komentar", review);
    formDataPayload.append("video", files[0]);

    setIsUploading(true);
    try {
      toast({
        description: "Uploading.. Please wait...",
        status: "info",
      });
      const response = await postCreateReview(
        formDataPayload,
        orderId,
        (progressEvent) => {
          const { loaded, total } = progressEvent;
          let percent = Math.floor((loaded * 100) / total);
          if (percent < 100) {
            setProgress(percent);
          }
        }
      );
      if (response && response.data.status === "Success") {
        setProgress(100);
        toast({
          description: response.data.message,
          status: "success",
        });
        router.push("/order/order-buyer/history");
      }
    } catch (error) {
      setProgress(null);
      if (!error?.response?.data?.error_description) {
        toast({
          description: "Internal Server Error",
          status: "error",
        });
      } else {
        error.response.data.error_description.forEach((message) =>
          toast({
            description: message,
            status: "error",
          })
        );
      }
    }

    setIsUploading(false);
  };

  console.log("mario progress =>", progress);

  return (
    <>
      <Head>Add Review</Head>
      <Flex height="100%" direction="column" padding="4rem">
        <Logo />
        <Flex direction="column" marginTop="6rem">
          <Box>
            <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
              Review
            </Text>
            <Form onChange={(e) => setReview(e.target.value)} />
          </Box>
          <Box style={{ marginTop: "1.5rem" }}>
            <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
              Video
            </Text>
            <FilePicker
              onFileChange={(f) => setFiles(f)}
              placeholder="Choose File"
              clearButtonLabel="Clear"
              accept="video/*"
              hideClearButton={false}
              multipleFiles={false}
            />
          </Box>
          {progress !== null && (
            <Box color={progress === 100 ? "#21BF73" : "#000000"}>
              Upload Progress: {progress}%
            </Box>
          )}
        </Flex>
        <Button
          marginTop="10rem"
          color="white"
          bg="primary"
          onClick={handleClick}
          _focus={{
            bg: "primary",
          }}
          _hover={{
            bg: "primary",
          }}
          _active={{
            bg: "primary",
          }}
          disabled={isUploading}
        >
          Kirim
        </Button>
      </Flex>
      <BottomNav />
    </>
  );
};

export default withAuth(OrderReview);
