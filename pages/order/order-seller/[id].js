import { useEffect, useRef, useState } from "react";
import Head from "next/head";
import Form from "../../../Components/Form";
import Logo from "../../../Components/Logo";
import Navbar from "../../../Components/Navbar";
import {
  Box,
  Button,
  Flex,
  NumberDecrementStepper,
  NumberIncrementStepper,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  Text,
  useToast,
} from "@chakra-ui/react";
import { useRouter } from "next/router";
import FilePicker from "chakra-ui-file-picker";
import Cookies from "js-cookie";
import {
  getAllFood,
  getSpecificFood,
  patchEditFood,
  postCreateFood,
} from "../../../services/food";

import withAuth from "../../../Components/Utils/withAuth";

const FoodEdit = () => {
  const toast = useToast();
  const router = useRouter();

  const [idMakanan, setIdMakanan] = useState(0);
  const [namaMenu, setNamaMenu] = useState("");
  const [deskripsiMenu, setDeskripsiMenu] = useState("");
  const [hargaSatuan, setHargaSatuan] = useState(0);
  const [file, setFile] = useState(null);
  const [buttonState, setButtonState] = useState(false);

  const accessToken = Cookies.get("accessToken");
  const userRole = Cookies.get("role");
  const usernameSeller = Cookies.get("username");

  const handlePageLoad = async (id) => {
    if (!accessToken) {
      await router.push("/login");
      toast({
        status: "warning",
        title: "You need to login first",
        position: "top",
      });
    }

    try {
      const response = await getSpecificFood(id);

      if (response.data.penjual !== usernameSeller) {
        await router.push("/food");
        toast({
          status: "warning",
          title: "You can't change other seller's food",
          position: "top",
        });
      }
      setIdMakanan(id);
      setNamaMenu(response.data.nama);
      setDeskripsiMenu(response.data.deskripsi);
      setHargaSatuan(response.data.harga);
      setFile(response.data.foto);
    } catch (error) {
      if (!error?.response?.data) {
        toast({
          status: "error",
          title: "Internal server error",
          position: "top",
        });
        return;
      }
      toast({
        status: "error",
        title: error.response.data.response,
        position: "top",
      });
    }
  };

  const handleFoodEdit = async () => {
    setButtonState(true);
    if (!file || file?.length < 1) {
      toast({
        status: "error",
        title: "foto tidak boleh kosong",
        position: "top",
      });
      setButtonState(false);
      return;
    }

    if (file.length > 1) {
      toast({
        status: "error",
        title: "foto tidak boleh melebihi 1",
        position: "top",
      });
      setButtonState(false);
      return;
    }
    const formData = new FormData();
    formData.append("nama", namaMenu);
    formData.append("harga", hargaSatuan);
    formData.append("deskripsi", deskripsiMenu);
    formData.append("foto", file[0]);
    formData.append("penjual", usernameSeller);

    try {
      const response = await patchEditFood(formData, idMakanan);
      toast({
        status: "success",
        title: "Successfully edited menu!",
        position: "top",
      });
      await router.push("/order/order-seller");
    } catch (error) {
      setButtonState(false);
      if (!error?.response?.data) {
        toast({
          status: "error",
          title: "Internal server error",
          position: "top",
        });
        return;
      }
      toast({
        status: "error",
        title: error.response.data.response,
        position: "top",
      });
    }
  };

  useEffect(() => {
    if (router.isReady) {
      handlePageLoad(router.query.id);
    }
  }, [router.isReady]);

  return (
    <>
      <Head>
        <title>Seller - Edit Menu</title>
      </Head>
      <Flex height="100%" direction="column" padding="4rem">
        <Logo />
        <Flex direction="column" marginTop="6rem">
          <Box>
            <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
              Nama Menu
            </Text>
            <Form
              value={namaMenu}
              onChange={(e) => setNamaMenu(e.target.value)}
            />
          </Box>
          <Box style={{ marginTop: "1.5rem" }}>
            <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
              Deskripsi Menu
            </Text>
            <Form
              value={deskripsiMenu}
              onChange={(e) => setDeskripsiMenu(e.target.value)}
            />
          </Box>
          <Box style={{ marginTop: "1.5rem" }}>
            <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
              Harga Satuan
            </Text>
            <NumberInput
              onChange={(valueString) => setHargaSatuan(valueString)}
              value={hargaSatuan}
              min={0}
            >
              <NumberInputField style={{ border: "1px solid #000000" }} />
              <NumberInputStepper>
                <NumberIncrementStepper />
                <NumberDecrementStepper />
              </NumberInputStepper>
            </NumberInput>
          </Box>
          <Box style={{ marginTop: "1.5rem" }}>
            <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
              Food Picture
            </Text>
            <FilePicker
              onFileChange={(f) => setFile(f)}
              placeholder="Choose File"
              clearButtonLabel="Clear"
              accept="image/*"
              hideClearButton={false}
              multipleFiles={false}
            />
          </Box>
        </Flex>
        <Button
          isLoading={buttonState}
          marginTop="10rem"
          color="white"
          bg="primary"
          onClick={handleFoodEdit}
        >
          Edit Menu
        </Button>
      </Flex>
      <Navbar />
    </>
  );
};

export default withAuth(FoodEdit);
