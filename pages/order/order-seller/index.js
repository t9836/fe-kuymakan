import React from "react";
import { Flex, Grid, useToast } from "@chakra-ui/react";
import Head from "next/head";
import { useState } from "react";
import Logo from "../../../Components/Logo";
import Card from "../../../Components/Seller/FoodList/Card";
import Navbar from "../../../Components/Navbar";
import Cookies from "js-cookie";
import { useRouter } from "next/router";
import { getAllFood, getAllSellerFood } from "../../../services/food";

import withAuth from "../../../Components/Utils/withAuth";

const OrderSeller = () => {
  const [foodlist, setFoodList] = useState([]);

  const accessToken = Cookies.get("accessToken");
  const username = Cookies.get("username");
  const userRole = Cookies.get("role");

  const toast = useToast();
  const router = useRouter();

  const handlePageLoad = async () => {
    if (!accessToken) {
      await router.push("/login");
      toast({
        status: "warning",
        title: "You need to login first",
        position: "top",
      });
    } else if (userRole !== "seller") {
      await router.push("/food");
      toast({
        status: "warning",
        title: "Only seller can create menu",
        position: "top",
      });
    }
  };

  const handleLoadFood = async () => {
    try {
      const response = await getAllSellerFood(username);
      setFoodList(response.data);
    } catch (error) {
      if (!error?.response?.data) {
        toast({
          status: "error",
          title: "Internal server error",
          position: "top",
        });
        return;
      }
      toast({
        status: "error",
        title: error.response.data.response,
        position: "top",
      });
    }
  };

  React.useEffect(() => {
    handlePageLoad();
    handleLoadFood();
  }, []);

  return (
    <>
      <Head>
        <title>Seller - Food</title>
      </Head>
      <Flex height="100%" direction="column" padding="4rem">
        <Logo />
        <Grid
          direction="column"
          marginTop="8"
          gridColumnGap={3}
          gridRowGap={6}
          gridTemplateColumns="1fr 1fr"
          placeItems={"center"}
        >
          {foodlist.map((food, key) => (
            <Card key={key} {...food} />
          ))}
        </Grid>
      </Flex>
      <Navbar />
    </>
  );
};

export default withAuth(OrderSeller);
