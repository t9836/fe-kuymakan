import { useEffect, useState } from "react";
import Head from "next/head";
import Form from "../../../Components/Form";
import Logo from "../../../Components/Logo";
import Navbar from "../../../Components/Navbar";
import FilePicker from "chakra-ui-file-picker";
import {
  Text,
  Flex,
  Box,
  Button,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
  useToast,
} from "@chakra-ui/react";
import { useRouter } from "next/router";
import { postCreateUser } from "../../../services/account";
import { postCreateFood } from "../../../services/food";
import Cookies from "js-cookie";

import withAuth from "../../../Components/Utils/withAuth";

const CreateMenu = () => {
  const [namaMenu, setNamaMenu] = useState("");
  const [deskripsiMenu, setDeskripsiMenu] = useState("");
  const [hargaSatuan, setHargaSatuan] = useState(0);
  const [file, setFile] = useState(null);
  const [buttonState, setButtonState] = useState(false);
  const accessToken = Cookies.get("accessToken");
  const userRole = Cookies.get("role");
  const usernameSeller = Cookies.get("username");

  const toast = useToast();
  const router = useRouter();

  const handlePageLoad = async () => {
    if (!accessToken) {
      await router.push("/login");
      toast({
        status: "warning",
        title: "You need to login first",
        position: "top",
      });
    } else if (userRole !== "seller") {
      await router.push("/food");
      toast({
        status: "warning",
        title: "Only seller can create menu",
        position: "top",
      });
    }
  };

  useEffect(() => {
    handlePageLoad();
  }, []);

  const handleCreateFood = async () => {
    setButtonState(true);

    if (!file || file?.length < 1) {
      toast({
        status: "error",
        title: "foto tidak boleh kosong",
        position: "top",
      });
      setButtonState(false);
      return;
    }

    if (file.length > 1) {
      toast({
        status: "error",
        title: "foto tidak boleh melebihi 1",
        position: "top",
      });
      setButtonState(false);
      return;
    }
    const formData = new FormData();
    formData.append("nama", namaMenu);
    formData.append("harga", hargaSatuan);
    formData.append("deskripsi", deskripsiMenu);
    formData.append("foto", file[0]);
    formData.append("penjual", usernameSeller);

    try {
      const response = await postCreateFood(formData);
      toast({
        status: "success",
        title: "Successfully added menu!",
        position: "top",
      });
      await router.push("/order/order-seller");
    } catch (error) {
      setButtonState(false);
      if (!error?.response?.data) {
        toast({
          status: "error",
          title: "Internal server error",
          position: "top",
        });
        return;
      }
      toast({
        status: "error",
        title: error.response.data.response,
        position: "top",
      });
    }
  };

  return (
    <>
      <Head>
        <title>Seller - Create Menu</title>
      </Head>
      <Flex height="100%" direction="column" padding="4rem">
        <Logo />
        <Flex direction="column" marginTop="6rem">
          <Box>
            <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
              Nama Menu
            </Text>
            <Form onChange={(e) => setNamaMenu(e.target.value)} />
          </Box>
          <Box style={{ marginTop: "1.5rem" }}>
            <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
              Deskripsi Menu
            </Text>
            <Form onChange={(e) => setDeskripsiMenu(e.target.value)} />
          </Box>
          <Box style={{ marginTop: "1.5rem" }}>
            <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
              Harga Satuan
            </Text>
            <NumberInput
              onChange={(valueString) => setHargaSatuan(valueString)}
              value={hargaSatuan}
              min={0}
            >
              <NumberInputField style={{ border: "1px solid #000000" }} />
              <NumberInputStepper>
                <NumberIncrementStepper />
                <NumberDecrementStepper />
              </NumberInputStepper>
            </NumberInput>
          </Box>
          <Box style={{ marginTop: "1.5rem" }}>
            <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
              Food Picture
            </Text>
            <FilePicker
              onFileChange={(f) => setFile(f)}
              placeholder="Choose File"
              clearButtonLabel="Clear"
              accept="image/*"
              hideClearButton={false}
              multipleFiles={false}
            />
          </Box>
        </Flex>
        <Button
          isLoading={buttonState}
          marginTop="10rem"
          color="white"
          bg="primary"
          onClick={handleCreateFood}
        >
          Buat Menu
        </Button>
      </Flex>
      <Navbar />
    </>
  );
};

export default withAuth(CreateMenu);
