import React from "react";
import Head from "next/head";
import Cookies from "js-cookie";
import { useRouter } from "next/router";
import Logo from "../../../Components/Logo";
import Navbar from "../../../Components/Navbar";
import { Flex, Spinner, Text, toast, useToast } from "@chakra-ui/react";
import { validateToken } from "../../../utils/token";
import { getAllOrderSeller } from "../../../services/order";
import Card from "../../../Components/Seller/OrderList/Card";

import withAuth from "../../../Components/Utils/withAuth";

const OrderList = () => {
  const [data, setData] = React.useState(null);
  const [isLoading, setIsLoading] = React.useState(true);
  const token = Cookies.get("accessToken");
  const role = Cookies.get("role");
  const router = useRouter();
  const toast = useToast();

  const getAllOrderData = async () => {
    try {
      const res = await getAllOrderSeller(Cookies.get("username"));
      const sortedData = res.data.data.sort(
        (item1, item2) => item2.id - item1.id
      );
      setData(sortedData);
      setIsLoading(false);
    } catch (err) {
      if (err.response.status === 403) {
        Cookies.remove("username");
        Cookies.remove("accessToken");
        Cookies.remove("role");
        Cookies.remove("refreshToken");

        toast({
          status: "error",
          title: "Credentials expired. Redirecting to login page",
          position: "top",
        });
        setIsLoading(false);

        return router.push("/login");
      } else {
        setIsLoading(false);
        return toast({
          status: "error",
          title: "Failed to fetch data",
          position: "top",
        });
      }
    }
  };

  React.useEffect(() => {
    if (!token & (role !== "seller")) {
      router.push("/login");
    }
    // else if (!validateToken()) {
    //     router.push('/login')
    // }
    getAllOrderData();
    console.log(data);
  }, []);

  return (
    <>
      <Head>
        <title>Seller - Order List</title>
      </Head>
      <Logo />
      <Flex justifyContent="center">{isLoading ? <Spinner /> : ""}</Flex>
      <Flex alignItems="center" flexDirection="column" paddingBottom="10rem">
        {data &&
          (data.length === 0 ? (
            <Flex justifyContent="center" alignItems="center">
              <Text
                fontFamily="Inter"
                fontWeight="400"
                marginTop={20}
                fontSize={24}
              >
                You have no order.
              </Text>
            </Flex>
          ) : (
            data.map((item, idx) => {
              return <Card key={idx} data={item} />;
            })
          ))}
      </Flex>
      <Navbar />
    </>
  );
};

export default withAuth(OrderList);
