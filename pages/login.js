import React, { useState } from "react";
import Form from "../Components/Form";
import Head from "next/head";
import Logo from "../Components/Logo";
import {
  Button,
  Box,
  Flex,
  Text,
  Input,
  InputGroup,
  InputRightElement,
  useToast,
} from "@chakra-ui/react";
import { useRouter } from "next/router";
import Cookies from "js-cookie";
import moment from "moment";

import { postLogin } from "../services/account";
import withoutAuth from "../Components/Utils/withoutAuth";

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [show, setShow] = React.useState(false);
  const [buttonState, setButtonState] = useState(false);

  const router = useRouter();
  const toast = useToast();

  const handleClick = () => setShow(!show);

  const handleLogin = async () => {
    setButtonState(true);
    const payload = {
      username,
      password,
    };

    try {
      const response = await postLogin(payload);
      const data = response.data;
      Cookies.set("accessToken", data.access_token);
      Cookies.set("refreshToken", data.refresh_token);
      Cookies.set("role", data.role);
      Cookies.set("username", username);
      Cookies.set("expiresIn", 300);
      Cookies.set("tokenAcquiredISOTime", moment().toISOString());

      toast({
        status: "success",
        title: "Successfully logged in!",
        position: "top",
      });

      if (data.role === "seller") router.push("/order/order-seller");
      else router.push("/food");
    } catch (error) {
      console.error(error);
      setButtonState(false);
      if (!error?.response?.data) {
        toast({
          status: "error",
          title: "Internal server error",
          position: "top",
        });
        return;
      }

      toast({
        status: "error",
        title: error.response.data.response,
        position: "top",
      });
    }
  };

  return (
    <>
      <Head>
        <title>Login</title>
      </Head>
      <Flex
        height="100%"
        direction="column"
        justifyContent="center"
        padding="4rem"
      >
        <Logo />
        <Flex direction="column">
          <Box marginTop="2.0rem">
            <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
              Username
            </Text>
            <Form onChange={(e) => setUsername(e.target.value)} />
          </Box>
          <Box marginTop="1.0rem">
            <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
              Password
            </Text>
            <InputGroup size="md">
              <Input
                borderColor="black"
                pr="4.5rem"
                type={show ? "text" : "password"}
                placeholder="Enter password"
                onChange={(e) => setPassword(e.target.value)}
              />
              <InputRightElement width="4.5rem">
                <Button h="1.75rem" size="sm" onClick={handleClick}>
                  {show ? "Hide" : "Show"}
                </Button>
              </InputRightElement>
            </InputGroup>
          </Box>
          <Button
            isLoading={buttonState}
            marginTop="1.5rem"
            color="white"
            bg="primary"
            onClick={handleLogin}
          >
            Login
          </Button>
        </Flex>
      </Flex>
    </>
  );
};

export default withoutAuth(Login);
