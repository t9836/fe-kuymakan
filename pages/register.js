import React, { useState } from "react";
import Form from "../Components/Form";
import Head from "next/head";
import FilePicker from "chakra-ui-file-picker";
import Logo from "../Components/Logo";
import { useRouter } from "next/router";
import {
  Button,
  Box,
  Flex,
  Text,
  Input,
  InputGroup,
  InputRightElement,
  useToast,
  FormControl,
  Select,
} from "@chakra-ui/react";
import Cookies from "js-cookie";
import moment from "moment";

import { postCreateUser } from "../services/account";

import withoutAuth from "../Components/Utils/withoutAuth";

const Register = () => {
  const [username, setUsername] = useState("");
  const [fullname, setFullname] = useState("");
  const [role, setRole] = useState("seller");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [password, setPassword] = useState("");
  const [show, setShow] = React.useState(false);
  const [showRetype, setShowRetype] = React.useState(false);
  const handleShowPassword = () => setShow(!show);
  const handleShowRetypePassword = () => setShowRetype(!showRetype);
  const [file, setFile] = useState(null);
  const [buttonState, setButtonState] = useState(false);

  const toast = useToast();
  const router = useRouter();

  const handleRegister = async () => {
    setButtonState(true);
    if (!file || file?.length < 1) {
      toast({
        status: "error",
        title: "Profile picture tidak boleh kosong",
        position: "top",
      });
      return;
    }

    if (file.length > 1) {
      toast({
        status: "error",
        title: "Profile picture tidak boleh melebihi 1",
        position: "top",
      });
      return;
    }

    const formData = new FormData();
    formData.append("username", username);
    formData.append("full_name", fullname);
    formData.append("role", role);
    formData.append("password", password);
    formData.append("profile_picture", file[0]);
    formData.append("phone_number", phoneNumber);

    try {
      const response = await postCreateUser(formData);
      toast({
        status: "success",
        title: "Successfully created user!",
        position: "top",
      });

      const data = response.data;

      Cookies.set("accessToken", data.access_token);
      Cookies.set("refreshToken", data.refresh_token);
      Cookies.set("role", data.role);
      Cookies.set("username", username);
      Cookies.set("expiresIn", 300);
      Cookies.set("tokenAcquiredISOTime", moment().toISOString());

      if (data.role === "seller") router.push("/order/order-seller");
      else router.push("/food");
    } catch (error) {
      setButtonState(false);
      if (!error?.response?.data) {
        toast({
          status: "error",
          title: "Internal server error",
          position: "top",
        });
        return;
      }
      const errors = Object.values(error.response.data);

      errors.forEach((error) =>
        error.forEach((err) =>
          toast({
            status: "error",
            title: err,
            position: "top",
          })
        )
      );
    }
  };

  return (
    <>
      <Head>
        <title>Register</title>
      </Head>
      <Flex height="100%" direction="column" justifyContent="center" px="4rem">
        <Logo />
        <Flex direction="column">
          <Box marginTop="2.0rem">
            <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
              Username
            </Text>
            <Form onChange={(e) => setUsername(e.target.value)} />
          </Box>
          <Box marginTop="1.0rem">
            <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
              Password
            </Text>
            <InputGroup size="md">
              <Input
                borderColor="black"
                pr="4.5rem"
                type={show ? "text" : "password"}
                placeholder="Enter password"
                onChange={(e) => setPassword(e.target.value)}
              />
              <InputRightElement width="4.5rem">
                <Button h="1.75rem" size="sm" onClick={handleShowPassword}>
                  {show ? "Hide" : "Show"}
                </Button>
              </InputRightElement>
            </InputGroup>
          </Box>
          <Box marginTop="1.0rem">
            <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
              Re-type Password
            </Text>
            <InputGroup size="md">
              <Input
                borderColor="black"
                pr="4.5rem"
                type={showRetype ? "text" : "password"}
                placeholder="Re-enter password"
              />
              <InputRightElement width="4.5rem">
                <Button
                  h="1.75rem"
                  size="sm"
                  onClick={handleShowRetypePassword}
                >
                  {showRetype ? "Hide" : "Show"}
                </Button>
              </InputRightElement>
            </InputGroup>
          </Box>
          <Box marginTop="1.0rem">
            <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
              Full name
            </Text>
            <Form onChange={(e) => setFullname(e.target.value)} />
          </Box>
          <Box marginTop="1.0rem">
            <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
              Role
            </Text>
            <FormControl>
              <Select value={role} onChange={(e) => setRole(e.target.value)}>
                <option value="seller">Seller</option>
                <option value="buyer">Buyer</option>
              </Select>
            </FormControl>
          </Box>
          <Box marginTop="1.0rem">
            <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
              Phone Number
            </Text>
            <Form onChange={(e) => setPhoneNumber(e.target.value)} />
          </Box>
          <Box style={{ marginTop: "1.5rem" }}>
            <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
              Profile Picture
            </Text>
            <FilePicker
              onFileChange={(f) => setFile(f)}
              placeholder="Choose File"
              clearButtonLabel="Clear"
              accept="image/*"
              hideClearButton={false}
              multipleFiles={false}
            />
          </Box>
          <Button
            isLoading={buttonState}
            marginTop="1.5rem"
            color="white"
            bg="primary"
            onClick={handleRegister}
          >
            Register
          </Button>
        </Flex>
      </Flex>
    </>
  );
};

export default withoutAuth(Register);
