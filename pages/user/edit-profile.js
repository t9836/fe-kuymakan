import React, { useState, useEffect } from "react";
import Head from "next/head";
import Form from "../../Components/Form";
import Logo from "../../Components/Logo";
import Navbar from "../../Components/Navbar";
import FilePicker from "chakra-ui-file-picker";
import styles from "/styles/Profile.module.css";
import Cookies from "js-cookie";
import { getUser } from "../../services/account";

import {
  Text,
  Flex,
  Box,
  Button,
  InputGroup,
  Input,
  InputRightElement,
  useToast,
} from "@chakra-ui/react";
import { useRouter } from "next/router";
import { patchUser } from "../../services/account";

import withAuth from "../../Components/Utils/withAuth";

const EditProfileMenu = () => {
  const usernameCookies = Cookies.get("username");

  const [username, setUsername] = useState("");
  const [fullName, setFullName] = useState("");
  const [nik, setNik] = useState("");
  const [password, setPassword] = useState("");
  const [file, setFile] = useState();
  const [fileUpload, setFileUpload] = useState();
  const [show, setShow] = React.useState(false);
  const [showRetype, setShowRetype] = React.useState(false);
  const handleShowPassword = () => setShow(!show);
  const handleShowRetypePassword = () => setShowRetype(!showRetype);
  const [buttonState, setButtonState] = useState(false);
  const [buttonStateUpdate, setButtonStateUpdate] = useState(false);

  const router = useRouter();
  const toast = useToast();

  useEffect(() => {
    if (!router.isReady) return;
    getUser(usernameCookies)
      .then((response) => {
        setUsername(response.data.username);
        setFullName(response.data.full_name);
        setNik(response.data.phone_number);
        setFile(response.data.profile_picture);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [router.isReady]);

  const handleClick = async () => {
    setButtonStateUpdate(true);
    const formData = new FormData();
    if (username || username?.length > 0) {
      formData.append("username", username);
    }
    if (fullName || fullName?.length > 0) {
      formData.append("full_name", fullName);
    }
    if (password || password?.length > 0) {
      formData.append("password", password);
    }
    if (fileUpload || fileUpload?.length === 1) {
      formData.append("profile_picture", fileUpload);
    }
    if (nik || nik?.length > 0) {
      formData.append("phone_number", nik);
    }
    try {
      const response = await patchUser(usernameCookies, formData);
      toast({
        status: "success",
        title: "Successfully updated user info!",
        position: "top",
      });
      setButtonStateUpdate(false);
    } catch (error) {
      console.log(error);
      setButtonStateUpdate(false);
      if (!error?.response?.data) {
        toast({
          status: "error",
          title: "Internal server error",
          position: "top",
        });
        return;
      }
      const errors = Object.values(error.response.data);

      errors.forEach((error) =>
        toast({
          status: "error",
          title: error,
          position: "top",
        })
      );
    }
  };

  const handleLogout = async () => {
    setButtonState(true);
    try {
      Cookies.remove("accessToken");
      Cookies.remove("refreshToken");
      Cookies.remove("role");
      Cookies.remove("username");
      router.push("/login");
    } catch (error) {
      setButtonState(false);
      if (!error?.response?.data) {
        toast({
          status: "error",
          title: "Internal server error",
          position: "top",
        });
        return;
      }

      toast({
        status: "error",
        title: error.response.data.response,
        position: "top",
      });
    }
  };

  return (
    <>
      <Head>
        <title>Edit Profile</title>
      </Head>
      <Flex height="100%" direction="column" padding="4rem">
        <Logo />
        <Flex direction="column" marginTop="2rem">
          <Flex justifyContent="center">
            <div className={`${styles.personContainer}`}>
              <img src={file} alt="" className={`${styles.imgPerson}`} />
            </div>
          </Flex>
          <Box mt="2rem">
            <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
              Username
            </Text>
            <Form
              onChange={(e) => setUsername(e.target.value)}
              value={username}
            />
          </Box>
          <Box marginTop="1.0rem">
            <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
              Password
            </Text>
            <InputGroup size="md">
              <Input
                borderColor="black"
                pr="4.5rem"
                type={show ? "text" : "password"}
                placeholder="Enter password"
                onChange={(e) => setPassword(e.target.value)}
              />
              <InputRightElement width="4.5rem">
                <Button h="1.75rem" size="sm" onClick={handleShowPassword}>
                  {show ? "Hide" : "Show"}
                </Button>
              </InputRightElement>
            </InputGroup>
          </Box>
          <Box marginTop="1.0rem">
            <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
              Re-type Password
            </Text>
            <InputGroup size="md">
              <Input
                borderColor="black"
                pr="4.5rem"
                type={showRetype ? "text" : "password"}
                placeholder="Re-enter password"
              />
              <InputRightElement width="4.5rem">
                <Button
                  h="1.75rem"
                  size="sm"
                  onClick={handleShowRetypePassword}
                >
                  {showRetype ? "Hide" : "Show"}
                </Button>
              </InputRightElement>
            </InputGroup>
          </Box>

          <Box style={{ marginTop: "1.5rem" }}>
            <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
              Full Name
            </Text>
            <Form
              onChange={(e) => setFullName(e.target.value)}
              value={fullName}
            />
          </Box>

          <Box style={{ marginTop: "1.5rem" }}>
            <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
              Nomor Handphone
            </Text>
            <Form onChange={(e) => setNik(e.target.value)} value={nik} />
          </Box>

          <Box style={{ marginTop: "1.5rem" }}>
            <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
              Profile Picture
            </Text>
            <FilePicker
              onFileChange={(f) => {
                if (f.length !== 0) {
                  setFile(URL.createObjectURL(f[0]));
                  setFileUpload(f[0]);
                } else {
                  setFile(null);
                }
              }}
              placeholder="Choose File"
              clearButtonLabel="Clear"
              accept="image/*"
              hideClearButton={false}
              multipleFiles={false}
            />
          </Box>
        </Flex>
        <Button
          isLoading={buttonStateUpdate}
          marginTop="5rem"
          color="white"
          bg="primary"
          onClick={handleClick}
        >
          Ubah
        </Button>
        <Button
          isLoading={buttonState}
          marginTop="1rem"
          color="white"
          bg="danger"
          onClick={handleLogout}
        >
          Log Out
        </Button>
      </Flex>
      <Navbar />
    </>
  );
};

export default withAuth(EditProfileMenu);
