import Head from "next/head";
import Logo from "../../../Components/Logo";
import BottomNav from "../../../Components/Navbar";
import {
  Flex,
  Box,
  Text,
  Image,
  Button,
  IconButton,
  useToast,
  Spinner,
} from "@chakra-ui/react";
import { BiMinusCircle, BiPlusCircle } from "react-icons/bi";
import { useState } from "react";
import { useRouter } from "next/router";
import { postCreateOrderBuyer } from "../../../services/order";
import Cookies from "js-cookie";
import axios from "axios";
import React from "react";
import currency from "currency.js";
import withAuth from "../../../Components/Utils/withAuth";

const OrderNow = () => {
  const router = useRouter();
  const id = router.query.id;
  const [count, setCount] = useState(1);
  const [isLoadingOrder, setIsLoadingOrder] = useState(false);
  const [dataFood, setDataFood] = useState(null);
  const token = Cookies.get("accessToken");
  const role = Cookies.get("role");
  const toast = useToast();
  

  const fetchFood = async () => {
    const options = {
      headers: {
        authorization: `Bearer ${token}`
      }
    }
    let foodData = await axios.get(
      `https://kuymakan-food.herokuapp.com/food/${router.query.id}`,
      options
    );

    foodData = foodData.data;
    return foodData;
  };

  React.useEffect(() => {
    const fetchFirstLoad = async () => {
      if (!token & (role !== "buyer")) {
        return router.push("/login");
      }
      const foodData = await fetchFood();
      setDataFood(foodData);
    };

    fetchFirstLoad();
  }, []);

  const handleClick = async () => {
    setIsLoadingOrder(true);
    try {
      const foodData = dataFood;
      const data = {
        username_buyer: Cookies.get("username"),
        username_seller: foodData.penjual,
        id_food: id,
        quantity: count,
      };
      let res = await postCreateOrderBuyer(data);
      console.log(res.data);
      setIsLoadingOrder(false);

      toast({
        title: "Success",
        description: "Successfully ordered.",
        status: "success",
        duration: 9000,
        isClosable: true,
        position: "top",
      });

      router.push("/food");
    } catch (err) {
      setIsLoadingOrder(false);
      return toast({
        title: "Failed",
        description: "Failed to order.",
        status: "error",
        duration: 9000,
        isClosable: true,
        position: "top",
      });
    }
  };

  if (dataFood === null) {
    return <Spinner />;
  }

  return (
    <>
      <Head>
        <title>Order Now</title>
      </Head>

      <Flex height="100%" direction="column" padding="4rem">
        <Logo />
        <Flex direction="column" marginTop="6rem">
          <Box width={"100%"}>
            <Flex h="100%" p="0">
              <Image
                maxHeight="6rem"
                maxWidth="6rem"
                src={dataFood.foto}
                alt="Dan Abramov"
              />
              <Flex
                pl="2"
                flexDirection="column"
                w="100%"
                h="100%"
                justifyContent={"space-around"}
              >
                <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
                  {dataFood.nama}
                </Text>
                <Text fontFamily="Inter" fontWeight={400} fontSize="1.25rem">
                  {currency(dataFood.harga, {
                    symbol: "Rp",
                    separator: ".",
                    precision: 0,
                  }).format()}
                </Text>
              </Flex>
              <Flex
                pl="2"
                w="100%"
                alignItems={"center"}
                justifyContent="space-around"
              >
                <IconButton
                  size="lg"
                  onClick={() =>
                    setCount((count) => (count === 1 ? 1 : count - 1))
                  }
                  icon={<BiMinusCircle size={30} />}
                />
                <Text fontFamily="Inter" fontWeight={400} fontSize="3rem">
                  {count}
                </Text>
                <IconButton
                  size="lg"
                  onClick={() => setCount((count) => count + 1)}
                  icon={<BiPlusCircle size={30} />}
                />
              </Flex>
            </Flex>
          </Box>
        </Flex>
        <Button
          marginTop="10rem"
          color="white"
          bg="primary"
          onClick={handleClick}
        >
          {isLoadingOrder ? <Spinner /> : "Order Now"}
        </Button>
      </Flex>
      <BottomNav />
    </>
  );
};

export default withAuth(OrderNow);
