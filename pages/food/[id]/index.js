import React, { useState, useEffect } from "react";
import Head from "next/head";
import {
  Flex,
  Box,
  Button,
  Text,
  Image,
  Heading,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
} from "@chakra-ui/react";
import currency from "currency.js";
import ReactPlayer from "react-player";
import Logo from "../../../Components/Logo";
import { useRouter } from "next/router";
import { getFoodDetail, getFoodReviews } from "../../../services/food";
import withAuth from "../../../Components/Utils/withAuth";

const FoodDetail = () => {
  const router = useRouter();
  const { id } = router.query;
  const [reviewIndex, setReviewIndex] = useState(null);

  const [data, setData] = useState(null);
  const [reviews, setReviews] = useState(null);

  useEffect(() => {
    if (!router.isReady) return;
    getFoodDetail(id)
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        console.error(error);
      });

    getFoodReviews(id)
      .then((response) => {
        if (response.data.length !== 0) {
          setReviews(response.data);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  }, [router.isReady, id]);

  const name = data?.nama;
  const price = data?.harga;
  const description = data?.deskripsi;
  const image = data?.foto;

  return (
    <>
      <Head>
        <title>Food - Detail</title>
      </Head>

      {data === null && (
        <p tw="flex justify-center items-center">Getting data . . .</p>
      )}

      {data !== null && (
        <>
          <Flex height="100%" direction="column" padding="4rem">
            <Logo />
            <Flex direction="column" marginTop="1rem">
              <Image
                src={image}
                alt={`Image for ${name}`}
                width="100%"
                height="120%"
                objectFit="cover"
              />
              <Flex mt="8" justifyContent="space-between" alignItems="center">
                <Heading fontSize="2xl">{name}</Heading>
                <Heading fontSize="xl">
                  {currency(price, {
                    symbol: "Rp",
                    separator: ".",
                    precision: 0,
                  }).format()}
                </Heading>
              </Flex>
              <Text mt="4">{description}</Text>
              <Box mt="12">
                <Heading fontSize="xl">Review</Heading>
                {reviews === null && (
                  <Flex justifyContent="center" padding="1rem" opacity={"50%"}>
                    <Text fontWeight="semibold" fontSize="xl">
                      Havent Been Reviewed
                    </Text>
                  </Flex>
                )}

                {reviews !== null && (
                  <>
                    <Flex overflowX="scroll" maxWidth="100%" mt="3">
                      {reviews.map((review, key) => (
                        <Box
                          padding="0"
                          position="relative"
                          marginRight="2"
                          flexShrink={0}
                          width={120}
                          height={90}
                          key={key}
                          cursor="pointer"
                          onClick={() => setReviewIndex(key)}
                        >
                          <Image
                            src={review.url_thumbnail}
                            alt={`Review ${key}`}
                            width="100%"
                            height="100%"
                            objectFit="cover"
                          />
                          <Image
                            src="/play.png"
                            alt="Play Button"
                            width="24px"
                            height="24px"
                            position="absolute"
                            zIndex={1}
                            top="calc(50% - 12px)"
                            left="calc(50% - 12px)"
                          />
                        </Box>
                      ))}
                    </Flex>
                  </>
                )}
              </Box>
            </Flex>
            <Button
              onClick={() => router.push(`/food/${router.query.id}/order-now`)}
              marginTop="4rem"
              color="white"
              bg="primary"
            >
              Tambah Pesanan
            </Button>
          </Flex>

          {reviews !== null && (
            <>
              <Modal
                isCentered
                isOpen={reviewIndex !== null}
                onClose={() => setReviewIndex(null)}
              >
                <ModalOverlay />
                <ModalContent>
                  <ModalBody padding={0}>
                    <Box position="relative" width="100%" paddingTop="56.25%">
                      {reviews[reviewIndex] && (
                        <ReactPlayer
                          width="100%"
                          height="100%"
                          style={{
                            position: "absolute",
                            top: 0,
                            left: 0,
                          }}
                          url={reviews[reviewIndex].url_video}
                          controls
                        />
                      )}
                    </Box>
                  </ModalBody>
                </ModalContent>
              </Modal>
            </>
          )}
        </>
      )}
    </>
  );
};

const dummyFoodDetail = {
  id: 1,
  name: "Batagor",
  description:
    "Makanan ini mengandung pangsit yang krispi, dengan saus kacang yang menggugah selera!",
  image: "/dummy-food-image3.png",
  price: 10000,
  reviews: [
    {
      id: 1,
      src: "https://www.youtube.com/watch?v=S40a4PNYdOw",
      thumbnail: "/dummy-food-image3.png",
    },
    {
      id: 2,
      src: "https://www.youtube.com/watch?v=S40a4PNYdOw",
      thumbnail: "/dummy-food-image3.png",
    },
    {
      id: 3,
      src: "https://www.youtube.com/watch?v=S40a4PNYdOw",
      thumbnail: "/dummy-food-image3.png",
    },
  ],
};

export default withAuth(FoodDetail);
