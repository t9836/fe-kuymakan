import React from "react";
import Head from "next/head";
import { Flex, Grid, Box, useToast } from "@chakra-ui/react";
import { useState } from "react";
import Logo from "../../Components/Logo";
import Card from "../../Components/Buyer/FoodList/Card";
import { useRouter } from "next/router";
import Cookies from "js-cookie";
import { getAllFood, postCreateFood } from "../../services/food";
import Navbar from "../../Components/Navbar";
import withAuth from "../../Components/Utils/withAuth";

const FoodList = () => {
  const [foodlist, setFoodList] = useState([]);

  const accessToken = Cookies.get("accessToken");

  const toast = useToast();
  const router = useRouter();

  const handlePageLoad = async () => {
    if (!accessToken) {
      await router.push("/login");
      toast({
        status: "warning",
        title: "You need to login first",
        position: "top",
      });
    }
  };

  const handleLoadFood = async () => {
    try {
      const response = await getAllFood();
      setFoodList(response.data);
    } catch (error) {
      if (!error?.response?.data) {
        toast({
          status: "error",
          title: "Internal server error",
          position: "top",
        });
        return;
      }
      toast({
        status: "error",
        title: error.response.data.response,
        position: "top",
      });
    }
  };

  React.useEffect(() => {
    handlePageLoad();
    handleLoadFood();
  }, []);

  return (
    <>
      <Head>
        <title>Food - List</title>
      </Head>
      <Flex height="100%" direction="column" padding="4rem">
        <Logo />
        <Grid
          direction="column"
          marginTop="8"
          gridColumnGap={3}
          gridRowGap={6}
          gridTemplateColumns="1fr 1fr"
          placeItems={"center"}
        >
          {foodlist.map((food, key) => (
            <Card key={key} {...food} />
          ))}
        </Grid>
      </Flex>
      <Navbar />
    </>
  );
};

export default withAuth(FoodList);
