import { ChakraProvider, extendTheme } from '@chakra-ui/react'
import Layout from '../Components/Layout'
import '../styles/globals.css'

const theme = extendTheme({
  colors: {
    primary: '#21BF73',
    secondary: '#B0EACD',
    white: '#F9FCFB',
    danger: '#FD5E53'
  }
})

function MyApp({ Component, pageProps }) {
  return (
    <ChakraProvider theme={theme}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </ChakraProvider>
  )
}

export default MyApp