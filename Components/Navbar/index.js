import styles from '/styles/Navbar.module.css'
import { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import { BsFillPersonFill } from "react-icons/bs";
import { GiHamburger, GiNotebook, GiCookingPot } from "react-icons/gi";
import { FaShoppingBasket, FaHistory } from "react-icons/fa";
import Cookies from "js-cookie";

const BottomNav = props => {
    const router = useRouter()
    const [activeTabs, setActiveTabs] = useState(props.name)
    const [showChef, setShowChef] = useState(false)
    const accessToken = Cookies.get("accessToken")
    const userRole = Cookies.get("role")

    const handlePageLoad = async () => {
        if(accessToken && userRole === 'seller') {
            setShowChef(true)
        }
    }

    useEffect(() => {
        handlePageLoad()
        switch (activeTabs) {
            case 'food':
                router.push('/food')
                break;
            case 'order':
                router.push('/order/order-seller/create-menu')
                break;
            case 'profile':
                router.push('/user/edit-profile')
                break;
            case 'chef':
                router.push('/order/order-seller')
                break;
            case 'order-list':
                router.push('/order/order-seller/order-list')
                break;
            case 'order-history-buyer':
                router.push('/order/order-buyer/history')
                break;
        }
    }, [activeTabs, router])

    return (
        <div className={`${styles.bottomNav}`}>
            {showChef === false ? 
                <div className={`${styles.bnTab}`} style={{cursor:'pointer'}}>
                    {activeTabs === 'food' ?
                        <GiHamburger
                            size='35'
                            color='#FF0000'
                            onClick={() => setActiveTabs('food')}
                        /> :
                        <GiHamburger
                            size='35'
                            color='#FFFFFF'
                            onClick={() => setActiveTabs('food')}
                        />}
                </div> 
            : null}

            {showChef === false ?
                <div className={`${styles.bnTab}`} style={{cursor:'pointer'}}>
                    {activeTabs === 'order-history-buyer' ?
                        <FaHistory
                            size='35'
                            color='#FF0000'
                            onClick={() => setActiveTabs('order-history-buyer')}
                        /> :
                        <FaHistory
                            size='35'
                            color='#FFFFFF'
                            onClick={() => setActiveTabs('order-history-buyer')}
                        />}
                </div>
            : null }

            {showChef === true?
                <div className={`${styles.bnTab}`} style={{cursor:'pointer'}}>
                    {activeTabs === 'order-list' ?
                        <FaShoppingBasket
                            size='35'
                            color='#FF0000'
                            onClick={() => setActiveTabs('order-list')}
                        /> :
                        <FaShoppingBasket
                            size='35'
                            color='#FFFFFF'
                            onClick={() => setActiveTabs('order-list')}
                        />}
                </div>
            : null }

            {showChef === true?
                <div className={`${styles.bnTab}`} style={{cursor:'pointer'}}>
                    {activeTabs === 'chef' ?
                        <GiCookingPot
                            size='35'
                            color='#FF0000'
                            onClick={() => setActiveTabs('chef')}
                        /> :
                        <GiCookingPot
                            size='35'
                            color='#FFFFFF'
                            onClick={() => setActiveTabs('chef')}
                        />}
                </div>
            : null }

            {showChef === true?
                <div className={`${styles.bnTab}`}  style={{cursor:'pointer'}}>
                    {activeTabs === 'order' ?
                        <GiNotebook
                            size='35'
                            color='#FF0000'
                            onClick={() => setActiveTabs('order')}
                        /> :
                        <GiNotebook
                            size='35'
                            color='#FFFFFF'
                            onClick={() => setActiveTabs('order')}
                        />}
                </div>
                : null }

            <div className={`${styles.bnTab}`}  style={{cursor:'pointer'}}>
                {activeTabs === 'profile' ?
                    <BsFillPersonFill
                        size='35'
                        color='#FF0000'
                        onClick={() => setActiveTabs('profile')}
                    /> :
                    <BsFillPersonFill
                        size='35'
                        color='#FFFFFF'
                        onClick={() => setActiveTabs('profile')}
                    />}
            </div>
        </div>
    )
}

export default BottomNav