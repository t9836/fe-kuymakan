import {
    Box,
    Flex,
    Text,
    Button,
    Image,
    useDisclosure,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton, useToast
} from "@chakra-ui/react"
import currency from "currency.js";
import Link from "next/link";
import {deleteFood, postCreateFood} from "../../../../services/food";
import {useRouter} from "next/router";
import {useState} from "react";


const textStyle = {
    fontFamily: 'Inter',
    fontWeight: '400',
    fontSize: '1rem'
}

const Card = ({ deskripsi, foto, harga, id, nama, penjual }) => {

    const { isOpen, onOpen, onClose } = useDisclosure()
    const [buttonState, setButtonState] = useState(false);
    const toast = useToast();
    const route = useRouter();

    const handleDeleteFood = async () => {
        setButtonState(true)
        try {
            const response = await deleteFood(id);
            toast({
                status: "success",
                title: "Successfully deleted menu!",
                position: "top",
            });
            setButtonState(false)
            route.reload()
        } catch (error) {
            setButtonState(false)
            if (!error?.response?.data) {
                toast({
                    status: "error",
                    title: "Internal server error",
                    position: "top",
                });
                return;
            }
            toast({
                status: "error",
                title: error.response.data.response,
                position: "top",
            });

        }
    }

    return (
        <Box
            width="100%"
            maxWidth="200px"
            borderRadius={10}
            overflow="hidden"
            filter="drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25))"
            bg="white"
        >
            <Image
                width="100%"
                height="104px"
                objectFit="cover"
                src={foto}
                alt={nama}
            />
            <Box p="2">
                <Flex justifyContent="space-between">
                    <Text fontSize="xs" mr="2">
                        {nama}
                    </Text>
                    <Text fontSize="xs">
                        {currency(harga, {
                            symbol: "Rp",
                            separator: ".",
                            precision: 0,
                        }).format()}
                    </Text>
                </Flex>
                <Link href={`/order/order-seller/${id}`}>
                    <Button
                        fontWeight="bold"
                        fontSize="xs"
                        paddingY={2}
                        width="100%"
                        marginTop="2"
                        color="white"
                        bg="primary"
                        _hover={{
                            bg: "primary",
                        }}
                    >
                        Edit
                    </Button>
                </Link>
                    <Button
                        fontWeight="bold"
                        fontSize="xs"
                        paddingY={2}
                        width="100%"
                        marginTop="2"
                        color="white"
                        bg="danger"
                        _hover={{
                            bg: "danger",
                        }}
                        onClick={onOpen}
                    >
                        Hapus
                    </Button>
                    <Modal onClose={onClose} isOpen={isOpen} isCentered>
                        <ModalOverlay />
                        <ModalContent>
                            <ModalHeader>Warning</ModalHeader>
                            <ModalCloseButton />
                            <ModalBody>
                                <p>Anda yakin ingin menghapus makanan ini?</p>
                            </ModalBody>
                            <ModalFooter>
                                <Button onClick={handleDeleteFood}
                                        bg="danger" _hover={{bg: "danger"}}
                                        color="white" marginRight={2} isLoading={buttonState}
                                >
                                    Hapus
                                </Button>
                                <Button onClick={onClose}>Close</Button>
                            </ModalFooter>
                        </ModalContent>
                    </Modal>
            </Box>
        </Box>
    );

};
export default Card;

