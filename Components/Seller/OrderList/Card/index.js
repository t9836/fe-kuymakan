import { Box, Flex, Button, Text, Tag, useToast } from "@chakra-ui/react"
import Image from "next/image"
import currency from "currency.js";
import {patchOrderSeller} from '../../../../services/order'
import React, {useState} from 'react';

export default function Card({data}) {

    const [orderData, setOrderData] = useState(data)
    const toast = useToast();

    const changeStatus = async (status) => {
        try {
            let res = await patchOrderSeller({id_order:data.id, status: status})
            res = res.data;
            setOrderData({
                ...orderData,
                status: status
            })
        } catch (err) {
            if (err.response.status === 403) {
                Cookies.remove('username')
                Cookies.remove('accessToken')
                Cookies.remove('role')
                Cookies.remove('refreshToken')
                
                toast({
                    status: "error",
                    title: "Credentials expired. Redirecting to login page",
                    position: "top",
                });
                setIsLoading(false);

                return router.push('/login')
            } else {
                setIsLoading(false);
                return toast({
                    status: "error",
                    title: "Failed to process food",
                    position: "top",
                });
            }
        }
    }

    return (
        <Box
            maxHeight='150px'
            border='1px solid #000'
            width='95%'
            overflow='hidden'
            marginTop='1rem'
            borderRadius='5px'
        >
            <Flex direction='row'>
                <Flex
                    height='100%'
                    width='35%'
                    alignItems='center'
                    // paddingBottom='10px'
                >
                    <Image src={orderData.image} width={150} height={150} />
                </Flex>
                <Flex
                    width='75%'
                    padding='10px'
                    direction='column'
                >
                    <Flex
                        direction='row'
                        justifyContent='space-between'
                        width='100%'
                    >
                        <Text fontFamily='Inter' fontSize='1.25rem'>{orderData.date.split(" ").slice(1,4).join(" ")}</Text>
                        <Flex gap='10px'>
                            {
                                orderData.status === 0 ? (
                                    <>
                                    <Button 
                                    onClick={() => changeStatus(1)} 
                                    borderRadius={10} bgColor='primary'color='white'>Proses</Button>
                                    
                                    <Button
                                        onClick={() => changeStatus(3)}
                                        borderRadius={10} 
                                        bgColor='danger' 
                                        color='white'
                                    >
                                        Batal
                                    </Button>
                                    </>
                                ) : orderData.status === 1 ? (
                                    <>
                                    <Button 
                                    onClick={() => changeStatus(2)} 
                                    borderRadius={10} bgColor='primary'color='white'>Selesaikan</Button>
                                    </>
                                ) : orderData.status === 2 ? (
                                    <>
                                        <Tag colorScheme='green'>Order Selesai</Tag>
                                    </>
                                ) : <Tag colorScheme='red'>Order dibatalkan</Tag>
                            }
                        </Flex>
                    </Flex>
                    <Text fontFamily='Inter' fontSize='1.5rem'>{orderData.name}</Text>
                    <Flex
                        justifyContent='space-between'
                    >
                        <Flex>
                            <Text fontFamily='Inter' fontSize='1.25rem'>{orderData.quantity} x &nbsp;</Text>
                            <Text color='primary' fontFamily='Inter' fontSize='1.25rem'>
                                {currency(orderData.price, {
                                    symbol: "Rp",
                                    separator: ".",
                                    precision: 0,
                                }).format()}
                            </Text>
                        </Flex>
                        <Text color='primary' fontSize='1.25rem'>
                            {/* Rp{orderData.quantity * orderData.price} */}
                            {currency(orderData.quantity*orderData.price, {
                            symbol: "Rp",
                            separator: ".",
                            precision: 0,
                        }).format()}
                        </Text>
                    </Flex>
                </Flex>

            </Flex>
        </Box>
    )
}