import React from "react";
import Cookies from "js-cookie";
import cookie from "cookie";
import ErrorPage from "next/error";
import moment from "moment";

const checkUserAuthentication = (req) => {
  /**
   * If in browser, then get cookie from the client side, thus using `js-cookies`
   * else, then parse cookie from the request header and parsing it, using `cookie`
   */

  let data = {};

  if (typeof window !== "undefined") {
    data["accessToken"] = Cookies.get("accessToken");
    data["refreshToken"] = Cookies.get("refreshToken");
    data["expiresIn"] = Cookies.get("expiresIn");
    data["tokenAcquiredISOTime"] = Cookies.get("tokenAcquiredISOTime");
  } else {
    const cookieData = cookie.parse(req?.headers?.cookie ?? "");
    data["accessToken"] = cookieData["accessToken"];
    data["refreshToken"] = cookieData["refreshToken"];
    data["expiresIn"] = cookieData["expiresIn"];
    data["tokenAcquiredISOTime"] = cookieData["tokenAcquiredISOTime"];
  }

  return data;
};

const withoutAuth = (WrappedComponent) => {
  const hocComponent = ({ ...props }) => {
    if (props.statusCode) {
      return <ErrorPage statusCode={props.statusCode} />;
    }
    return <WrappedComponent {...props} />;
  };

  hocComponent.getInitialProps = async (ctx) => {
    const { req } = ctx;
    const { accessToken, refreshToken, expiresIn, tokenAcquiredISOTime } =
      checkUserAuthentication(req);

    let flag = true;

    if (!(expiresIn && tokenAcquiredISOTime && accessToken && refreshToken)) {
      flag = false;
    } else {
      const expiredTime = moment(tokenAcquiredISOTime).add(
        parseInt(expiresIn),
        "seconds"
      );
      const currentTime = moment();

      if (currentTime.isSameOrAfter(expiredTime)) {
        flag = false;
      }
    }

    if (flag) {
      return { statusCode: 404 };
    }

    if (WrappedComponent.getInitialProps) {
      const wrappedProps = await WrappedComponent.getInitialProps(ctx);
      return { ...wrappedProps };
    }

    return {};
  };

  return hocComponent;
};

export default withoutAuth;
