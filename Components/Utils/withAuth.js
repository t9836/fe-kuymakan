import React from "react";
import Router from "next/router";
import Cookies from "js-cookie";
import cookie from "cookie";

import moment from "moment";

const checkUserAuthentication = (req) => {
  /**
   * If in browser, then get cookie from the client side, thus using `js-cookies`
   * else, then parse cookie from the request header and parsing it, using `cookie`
   */

  let data = {};

  if (typeof window !== "undefined") {
    data["accessToken"] = Cookies.get("accessToken");
    data["refreshToken"] = Cookies.get("refreshToken");
    data["expiresIn"] = Cookies.get("expiresIn");
    data["tokenAcquiredISOTime"] = Cookies.get("tokenAcquiredISOTime");
  } else {
    const cookieData = cookie.parse(req?.headers?.cookie ?? "");
    data["accessToken"] = cookieData["accessToken"];
    data["refreshToken"] = cookieData["refreshToken"];
    data["expiresIn"] = cookieData["expiresIn"];
    data["tokenAcquiredISOTime"] = cookieData["tokenAcquiredISOTime"];
  }

  return data;
};

const loginURL = "/";
const withAuth = (WrappedComponent) => {
  const hocComponent = ({ ...props }) => <WrappedComponent {...props} />;

  hocComponent.getInitialProps = async (ctx) => {
    const { req, res } = ctx;
    const { accessToken, refreshToken, expiresIn, tokenAcquiredISOTime } =
      checkUserAuthentication(req);

    let flag = true;

    if (expiresIn && tokenAcquiredISOTime && accessToken && refreshToken) {
      const expiredTime = moment(tokenAcquiredISOTime).add(
        parseInt(expiresIn),
        "seconds"
      );
      const currentTime = moment();

      if (currentTime.isBefore(expiredTime)) {
        flag = false;
      }
    }

    const returnProps = {
      accessToken,
      refreshToken,
    };

    if (flag) {
      if (res) {
        res.writeHead(302, {
          location: loginURL,
        });
        res.end();
      } else {
        Router.push(loginURL);
      }
    }

    if (WrappedComponent.getInitialProps) {
      const wrappedProps = await WrappedComponent.getInitialProps(ctx);
      return { ...wrappedProps, ...returnProps };
    }

    return returnProps;
  };

  return hocComponent;
};

export default withAuth;
