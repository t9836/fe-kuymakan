import React from "react";
import { Box, Flex, Text, Image, Button } from "@chakra-ui/react";
import currency from "currency.js";
import Link from "next/link";

const Card = ({ deskripsi, foto, harga, id, nama, penjual }) => {

  return (
    <Box
      width="100%"
      maxWidth="200px"
      borderRadius={10}
      overflow="hidden"
      filter="drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25))"
      bg="white"
    >
      <Image
        width="100%"
        height="104px"
        objectFit="cover"
        src={foto}
        alt={nama}
      />
      <Box p="2">
        <Flex justifyContent="space-between">
          <Text fontSize="xs" mr="2">
            {nama}
          </Text>
          <Text fontSize="xs">
            {currency(harga, {
              symbol: "Rp",
              separator: ".",
              precision: 0,
            }).format()}
          </Text>
        </Flex>
        <Link href={`/food/${id}`}>
          <Button
            fontWeight="bold"
            fontSize="xs"
            paddingY={2}
            width="100%"
            marginTop="2"
            color="white"
            bg="primary"
            _hover={{
              bg: "primary",
            }}
          >
            Pesan
          </Button>
        </Link>
      </Box>
    </Box>
  );
};

export default Card;
