import {Flex, Image} from '@chakra-ui/react'

export default function Person() {
    return (
        <Flex className='personContainer' justifyContent='center' marginY='30'>
           <Image src='/person.png' alt='person'/>
        </Flex>
    )
}