import { 
    Box,
    Button,
    Flex,
    Image,
    Text,
    Spinner,
    color
} from "@chakra-ui/react";
import axios from 'axios';
import Cookies from "js-cookie";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";

const statusMap = {
    0: "Dipesan",
    1: "Diproses",
    2: "Selesai",
    3: "Gagal"
}

const statusColorMap = {
    0: "#CCCC00",
    1: "blue",
    2: "green",
    3: "red"
}

const OrderHistoryCard = (props) => {

    const [foodImage, setFoodImage] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const router = useRouter();

    const getFoodImage = async () => {
        const options = {
            headers: {
                "Authorization" : `Bearer ${Cookies.get('accessToken')}`
            }
        }
        let response = await axios.get(`https://kuymakan-food.herokuapp.com/food/${props.data.id_food}`, options)
        setFoodImage(response.data.foto)
        setIsLoading(false);
    }

    const goToReviewPage = () => {
        router.push(`/order/order-buyer/history/${props.data.id}/review`)
    }

    useEffect(() => {
        getFoodImage();
    }, [])

    return(
        <Flex mt="3">
            <Box 
            borderStyle='solid'
            borderColor='black'
            borderRadius='10px'
            borderWidth='1px'
            p='2'
            width={"100%"}>
                <Flex>
                    {isLoading ? <Spinner /> : <Image maxHeight='110px' maxWidth='110px' src={foodImage} alt='Dan Abramov' />}
                    <Box px='2' w="100%" h="110px">
                        <Flex flexDirection="column" justifyContent={"space-between"}>
                            <Flex flexDirection='row' justifyContent={"space-between"}>
                                <Text fontSize="20px">{props.data.date.split(" ").slice(1,4).join(" ")}</Text>
                                <Text fontSize="20px" color={statusColorMap[props.data.status]} fontWeight="bold">
                                    {statusMap[props.data.status]}
                                </Text>
                            </Flex>
                            <Text fontSize="24px" fontWeight='extrabold'>{props.data.name}</Text>
                            <Flex flexDirection='row' justifyContent={"space-between"}>
                                <Text fontSize="20px">{props.data.quantity}x Rp.{props.data.price}</Text>
                                <Text fontSize="20px">Rp.{props.data.quantity * props.data.price}</Text>
                            </Flex>
                        </Flex>
                    </Box>
                </Flex>
                
                {
                    !props.data.is_reviewed && props.data.status === 2 ? 
                    <Button 
                        mt={3} 
                        width="100%" 
                        bgColor='primary' 
                        color='white'
                        onClick={() => goToReviewPage()}
                    >
                        Tambah Review
                    </Button> : ""
                }
            </Box>
        </Flex>
    )
};

export default OrderHistoryCard;