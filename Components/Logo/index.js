import { Text } from '@chakra-ui/react'

export default function Logo() {
    return (
        <Text fontSize='3rem' textAlign='center' fontFamily='freehand'>
            KuyMakan
        </Text>
    )
}