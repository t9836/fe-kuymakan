import { Input } from '@chakra-ui/react';

const inputStyle = {
    border: '1px solid #000000',
}

export default function Form(props) {
    return (
        <Input style={inputStyle} {...props} size='md' />
    )
}