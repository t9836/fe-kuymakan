import axios from "axios";

import endpoint from "./endpoint";
import Cookies from "js-cookie";

export const postCreateUser = (data) => {
  const options = {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  };
  return axios.post(endpoint.createUser, data, options);
};

export const postLogin = (data) => {
  const options = {
    headers: {
      "Content-Type": "application/json",
    },
  };
  return axios.post(endpoint.login, data, options);
};

export const getUser = (username) => {
  const options = {
    headers: {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${Cookies.get('accessToken')}`
    },
  };
  return axios.get(endpoint.createUser+username, options);
};

export const patchUser = (username, data) => {
  const options = {
    headers: {
      "Content-Type": "multipart/form-data",
      "Authorization": `Bearer ${Cookies.get('accessToken')}`
    },
  };
  return axios.patch(endpoint.createUser+username, data, options);
};