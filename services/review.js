import axios from "axios";
import endpoint from "./endpoint";

import Cookies from "js-cookie";

export const postCreateReview = (data, orderId, onUploadProgress) => {
  const options = {
    headers: {
      "Content-Type": "multipart/form-data",
      Authorization: `Bearer ${Cookies.get("accessToken")}`,
    },
    onUploadProgress,
  };
  return axios.post(endpoint.postReviewOrder + orderId, data, options);
};
