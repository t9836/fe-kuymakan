import axios from "axios";
import https from 'https';
import endpoint from "./endpoint";
import Cookies from 'js-cookie'

axios.defaults.httpsAgent = new https.Agent({
  rejectUnauthorized: false
})

export const getAllOrderSeller = (data) => {
    const options = {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${Cookies.get('accessToken')}`
      },
      params: {
        username_seller: data
      }
    };
  
    return axios.get(endpoint.sellerOrder, options)
}

export const patchOrderSeller = (data) => {
    const options = {
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${Cookies.get('accessToken')}`
        },
    };

    return axios.patch(endpoint.sellerOrder, data, options)
}

export const postCreateOrderBuyer = (data) => {
  const options = {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${Cookies.get('accessToken')}`
      },
  };

  return axios.post(endpoint.buyerOrder, data, options)
}


export const getAllOrderBuyer = (data) => {
  const options = {
    headers: {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${Cookies.get('accessToken')}`
    },
    params: {
      username_buyer: data
    }
  };

  return axios.get(endpoint.buyerOrder + "/history", options)
}