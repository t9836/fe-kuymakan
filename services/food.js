import Cookies from "js-cookie";
import axios from "axios";
import endpoint from "./endpoint";

export const getFoodDetail = (id) => {
  const options = {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${Cookies.get("accessToken")}`,
    },
  };
  return axios.get(endpoint.foods + "/" + id, options);
};

export const getFoodReviews = (id) => {
  const options = {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${Cookies.get("accessToken")}`,
    },
  };
  return axios.get(endpoint.foodReview + "/" + id, options);
};

export const postCreateFood = (data) => {
  const options = {
    headers: {
      "Content-Type": "multipart/form-data",
      Authorization: `Bearer ${Cookies.get("accessToken")}`,
    },
  };
  return axios.post(endpoint.food, data, options);
};

export const getAllFood = () => {
  const options = {
    headers: {
      "Content-Type": "multipart/form-data",
      Authorization: `Bearer ${Cookies.get("accessToken")}`,
    },
  };
  return axios.get(endpoint.food, options);
};

export const getSpecificFood = (id) => {
  const getUrl = endpoint.food + id;

  const options = {
    headers: {
      "Content-Type": "multipart/form-data",
      Authorization: `Bearer ${Cookies.get("accessToken")}`,
    },
  };
  return axios.get(getUrl, options);
};

export const patchEditFood = (data, id) => {
  const editUrl = endpoint.food + id;

  const options = {
    headers: {
      "Content-Type": "multipart/form-data",
      Authorization: `Bearer ${Cookies.get("accessToken")}`,
    },
  };
  return axios.patch(editUrl, data, options);
};

export const getAllSellerFood = (data) => {
  const sellerUrl = endpoint.food + "seller?penjual=" + data;

  const options = {
    headers: {
      "Content-Type": "multipart/form-data",
      Authorization: `Bearer ${Cookies.get("accessToken")}`,
    },
  };

  return axios.get(sellerUrl, options);
};

export const deleteFood = (id) => {
  const deleteUrl = endpoint.food + id;

  const options = {
    headers: {
      "Content-Type": "multipart/form-data",
      Authorization: `Bearer ${Cookies.get("accessToken")}`,
    },
  };

  return axios.delete(deleteUrl, options);
};
