const accountBaseURL = process.env.NEXT_PUBLIC_API_ACCOUNT_URL;
const oauthBaseURL = process.env.NEXT_PUBLIC_API_OAUTH_URL;
const foodBaseURL = process.env.NEXT_PUBLIC_API_FOOD_URL;
const orderBaseURL = process.env.NEXT_PUBLIC_API_ORDER_URL;
const reviewBaseURL = foodBaseURL;

const endpoint = {
  // account related endpoints
  createUser: accountBaseURL + "/user/",
  login: accountBaseURL + "/login/",

  // Order related endpoints
  sellerOrder: orderBaseURL + "/order/seller",
  buyerOrder: orderBaseURL + "/order/buyer",
  food: foodBaseURL + "/food/",

  // Review related endpoints
  postReviewOrder: reviewBaseURL + "/review/order/",

  // food related endpoint
  foods: foodBaseURL + "/food",
  foodReview: foodBaseURL + "/review/food",
};

export default endpoint;
