import Cookies from "js-cookie"
import axios from 'axios';

// Util function for validating token
export const validateToken = async () => {
    const access = Cookies.get('accessToken');
    const refresh = Cookies.get('refreshToken');

    const options = {
        headers: {
            'Authorization': 'Bearer ' + access,
            "Content-Type": "application/json",
        }
    }

    try {
        const response = await axios.post('https://oauth-a7law.herokuapp.com/oauth/resource', {}, options);
        return true;
    } catch (err) {
        try {
            const response = await axios.post('https://oauth-a7law.herokuapp.com/oauth/token/refresh', {
                refresh_token: refresh
            }, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });

            Cookies.set('accessToken', response.data.access_token)
            Cookies.set('refreshToken', response.data.refresh_token)
        } catch (err) {
            return false;
        }
    }
}